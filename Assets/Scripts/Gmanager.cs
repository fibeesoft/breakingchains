﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Gmanager : MonoBehaviour
{
    public static Gmanager instance;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    [SerializeField] GameObject MenuPanel;
    public static bool isDead = false;
    void Start()
    {
        OpenMenu();
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);

    }

    public void OpenMenu()
    {
        MenuPanel.SetActive(true);
        Time.timeScale = 0f;
    }

    public void CLoseMenu()
    {
        MenuPanel.SetActive(false);
        Time.timeScale = 1f;
    }

    public void QuitTheGame()
    {
        Application.Quit();
    }

}
