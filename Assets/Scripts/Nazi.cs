﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nazi : MonoBehaviour
{
    Rigidbody2D rb;
    float horSpeed;

    [SerializeField] GameObject bulletPref;
    [SerializeField] GameObject Aim;
    float HP = 5f;
    bool isWalking = true;
    float shootingSpeed = 1.5f;
    [SerializeField] GameObject muzzlePref;
    [SerializeField] GameObject takeDamagePref;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        horSpeed = -20f;
        InvokeRepeating("Shoot", 2.0f, shootingSpeed);
    }

    private void Update()
    {
        if (isWalking)
        {
            Move();
        }
    }

    public void Move()
    {
        rb.velocity = new Vector2(horSpeed * Time.deltaTime, 0);
    }



    public void Shoot()
    {
        GameObject bullet = Instantiate(bulletPref, Aim.transform.position, Quaternion.identity);
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(-2000 * Time.deltaTime, 0);
        Destroy(bullet, 4);
        GameObject muzzleEf = Instantiate(muzzlePref, Aim.transform.position, Quaternion.Euler(0, 0, -90f));
        Destroy(muzzleEf, 0.3f);
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == ("Bulletpl"))
        {
            TakeDamage();
            GameObject damage = Instantiate(takeDamagePref, col.transform.position, Quaternion.identity);
            Destroy(damage, 0.3f);
            Destroy(col.gameObject);
            print(HP);
        }
    }

    public void TakeDamage()
    {
        if (HP > 0)
        {
            HP -= 1;

        }
        else
        {
            Destroy(gameObject);
        }
    }

}
