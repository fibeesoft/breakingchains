﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] GameObject playerAnim;
    Rigidbody2D rb;
    float horSpeed, verSpeed;
    float verMaxPos = 1f;
    float verMinPos = -8f;
    public Joystick joystick;
    [SerializeField] GameObject bulletPref, muzzlePref;
    [SerializeField] GameObject soldierAim;
    [SerializeField] GameObject HpBarParent;
    [SerializeField] GameObject HpBar;
    [SerializeField] GameObject takeDamagePref;
    GameObject[] hpBarArray;
    float HP = 5;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        hpBarArray = new GameObject[(int)(HP)];
        HpBarIndicate();
    }

    public void HpBarIndicate()
    {
        if (hpBarArray.Length > 0)
        {
            foreach (GameObject b in hpBarArray)
            {
                Destroy(b);
            }
        }

        float hpBarPosX = -90;
        for (int i = 0; i < HP; i++)
        {
            GameObject bar = Instantiate(HpBar, new Vector2(hpBarPosX, 0), Quaternion.identity);
            bar.gameObject.transform.SetParent(HpBarParent.GetComponent<Transform>(), false);
            hpBarArray[i] = bar;
            hpBarPosX += 50f;
        }
    }

    private void Update()
    {
        //horSpeed = Input.GetAxis("Horizontal");
        horSpeed = joystick.Horizontal;
        //verSpeed = Input.GetAxis("Vertical");
        verSpeed = joystick.Vertical;
        if (horSpeed >= 0)
        {
            playerAnim.GetComponent<SpriteRenderer>().flipX = false;
        }
        else
        {
            playerAnim.GetComponent<SpriteRenderer>().flipX = true;
        }

        if (transform.position.y < verMaxPos && transform.position.y > verMinPos && transform.position.x > -25f)
        {
            Move();
        }
        else
        {
            if (transform.position.y >= verMaxPos)
            {
                transform.position = new Vector2(transform.position.x, verMaxPos - 0.05f);
            }
            else if (transform.position.y <= verMinPos)
            {
                transform.position = new Vector2(transform.position.x, verMinPos + 0.05f);
            }
            else if (transform.position.x <= -25f)
            {
                transform.position = new Vector2(-24.95f, transform.position.y);
            }
        }

    }

    public void Move()
    {
        rb.velocity = new Vector2(horSpeed * 500f * Time.deltaTime, verSpeed * 300 * Time.deltaTime);
    }

    public void Shoot()
    {
        GameObject bullet = Instantiate(bulletPref, soldierAim.transform.position, Quaternion.identity);
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(2000 * Time.deltaTime, 0);
        Destroy(bullet, 3);
        GameObject muzzleEf = Instantiate(muzzlePref, soldierAim.transform.position, Quaternion.Euler(0, 0, 90f));
        Destroy(muzzleEf, 0.3f);
    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "FinishLine")
        {
            Gmanager.instance.Restart();
            print("Wygrana");
        }
        if (collision.gameObject.tag == "Bullet")
        {
            TakeDamage();
            GameObject damage = Instantiate(takeDamagePref, collision.transform.position, Quaternion.identity);
            Destroy(damage, 0.3f);
            Destroy(collision.gameObject);
        }
    }



    public void TakeDamage()
    {
        if (HP > 0)
        {
            HP -= 1;
            HpBarIndicate();
        }
        else
        {
            StartCoroutine("Die");
        }
    }


    public IEnumerator Die()
    {
        print("a");
        yield return new WaitForSeconds(2f);
        Gmanager.instance.Restart();
        Destroy(gameObject);
        print("b");
    }

}
