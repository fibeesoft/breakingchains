﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    float Hp = 15f;
    [SerializeField] GameObject explPref, smokePref;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Bullet" || collision.gameObject.tag == "Bulletpl")
        {
            Destroy(collision.gameObject);

            TakeDamage();
         

        }
    }

    public void TakeDamage()
    {
        if (Hp > 0)
        {
            Hp -= 1;
        }
        else
        {
            Destroy(gameObject);
            GameObject explosion = Instantiate(explPref, transform.position, Quaternion.identity);
            Destroy(explosion, 0.5f);
            GameObject smExplosion = Instantiate(smokePref, transform.position, Quaternion.identity);
            Destroy(smExplosion, 1f);
        }
    }
}
